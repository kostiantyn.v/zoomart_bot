const emailQuery = `SELECT user_email, display_name, role.meta_value AS 'role' FROM wp_users AS users
INNER JOIN wp_usermeta AS usermeta ON users.ID = usermeta.user_id AND usermeta.meta_key = 'wp_capabilities'
INNER JOIN wp_usermeta AS role ON users.ID = role.user_id AND role.meta_key = 'wp_capabilities'`;

const processingProductQuery = `SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value  END ) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value  END ) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value  END ) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value  END ) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value  END ) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value  END ) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' THEN postmeta.meta_value  END ) AS 'time',
MAX(CASE WHEN postmeta.meta_key = '_order_total' THEN postmeta.meta_value END) AS 'price'

FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id 

INNER JOIN wp_woocommerce_order_items AS order_name
ON
orders.ID = order_name.order_id 

WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-processing'
GROUP BY ID`;

const assemblyProductQuery = `SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value  END ) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value  END ) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value  END ) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value  END ) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value  END ) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value  END ) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' THEN postmeta.meta_value  END ) AS 'time',
MAX(CASE WHEN postmeta.meta_key = '_order_total' THEN postmeta.meta_value END) AS 'price'

FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id 

INNER JOIN wp_woocommerce_order_items AS order_name
ON
orders.ID = order_name.order_id 

WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-packing'
GROUP BY ID`;

const readyToDelivery = `SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value  END ) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value  END ) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value  END ) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value  END ) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value  END ) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value  END ) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' THEN postmeta.meta_value  END ) AS 'time',
MAX(CASE WHEN postmeta.meta_key = '_order_total' THEN postmeta.meta_value END) AS 'price'

FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id 

INNER JOIN wp_woocommerce_order_items AS order_name
ON
orders.ID = order_name.order_id 

WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-ready-to-delivery'
GROUP BY ID`;

const selectCancelledItemsQuery = `
SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type

FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id 

INNER JOIN wp_woocommerce_order_items AS order_name
ON
orders.ID = order_name.order_id 

WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-cancelled'
GROUP BY ID
`;

const countStatusesQuery = `
SELECT DISTINCT 
COUNT(CASE WHEN post_status = 'wc-cancelled' THEN post_status END) AS 'cancelled',
COUNT(CASE WHEN post_status = 'wc-packing' THEN post_status END) AS 'packing',
COUNT(CASE WHEN post_status = 'wc-transportation' THEN post_status END) AS 'transporting',
COUNT(CASE WHEN post_status = 'wc-completed' THEN post_status END) AS 'completed'
FROM wp_posts WHERE post_type = 'shop_order'
`;

const readyToDeliveryBy10To12 = `
SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '10:00' AND '12:00' THEN postmeta.meta_value END) AS 'time',
MAX(CASE WHEN postmeta.meta_key = '_order_total' THEN postmeta.meta_value END) AS 'price'
      
FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id
      
INNER JOIN wp_woocommerce_order_items AS order_name
ON orders.ID = order_name.order_id
      
WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-ready-to-delivery'
GROUP BY ID
`;

const readyToDeliveryBy12To14 = `
SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '12:00' AND '14:00' THEN postmeta.meta_value END) AS 'time',
MAX(CASE WHEN postmeta.meta_key = '_order_total' THEN postmeta.meta_value END) AS 'price'
      
FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id
      
INNER JOIN wp_woocommerce_order_items AS order_name
ON orders.ID = order_name.order_id
      
WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-ready-to-delivery'
GROUP BY ID
`;

const readyToDeliveryBy14To16 = `
SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '14:00' AND '16:00' THEN postmeta.meta_value END) AS 'time',
MAX(CASE WHEN postmeta.meta_key = '_order_total' THEN postmeta.meta_value END) AS 'price'
      
FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id
      
INNER JOIN wp_woocommerce_order_items AS order_name
ON orders.ID = order_name.order_id
      
WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-ready-to-delivery'
GROUP BY ID
`;

const readyToDeliveryBy16To18 = `
SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '16:00' AND '18:00' THEN postmeta.meta_value END) AS 'time',
MAX(CASE WHEN postmeta.meta_key = '_order_total' THEN postmeta.meta_value END) AS 'price'
      
FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id
      
INNER JOIN wp_woocommerce_order_items AS order_name
ON orders.ID = order_name.order_id
      
WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-ready-to-delivery'
GROUP BY ID
`;

const readyToDeliveryBy18To20 = `
SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '18:00' AND '20:00' THEN postmeta.meta_value END) AS 'time',
MAX(CASE WHEN postmeta.meta_key = '_order_total' THEN postmeta.meta_value END) AS 'price'
      
FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id
      
INNER JOIN wp_woocommerce_order_items AS order_name
ON orders.ID = order_name.order_id
      
WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-ready-to-delivery'
GROUP BY ID
`;

const readyToDeliveryBy20To22 = `
SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value END) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value END) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value END) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value END) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value END) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value END) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' AND postmeta.meta_value BETWEEN '20:00' AND '22:00' THEN postmeta.meta_value END) AS 'time',
MAX(CASE WHEN postmeta.meta_key = '_order_total' THEN postmeta.meta_value END) AS 'price'
      
FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id
      
INNER JOIN wp_woocommerce_order_items AS order_name
ON orders.ID = order_name.order_id
      
WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-ready-to-delivery'
GROUP BY ID
`;

const failedOrdersQuery = `
SELECT DISTINCT
orders.ID,
orders.post_status,
orders.post_type,
order_name.order_item_name AS 'order_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_first_name' THEN postmeta.meta_value  END ) AS 'first_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_last_name' THEN postmeta.meta_value  END ) AS 'last_name',
MAX(CASE WHEN postmeta.meta_key = '_billing_phone' THEN postmeta.meta_value  END ) AS 'phone',
MAX(CASE WHEN postmeta.meta_key = '_billing_email' THEN postmeta.meta_value  END ) AS 'email',
MAX(CASE WHEN postmeta.meta_key = '_billing_address_1' THEN postmeta.meta_value  END ) AS 'address',
MAX(CASE WHEN postmeta.meta_key = 'delivery_date' THEN postmeta.meta_value  END ) AS 'date',
MAX(CASE WHEN postmeta.meta_key = 'delivery_time' THEN postmeta.meta_value  END ) AS 'time',
MAX(CASE WHEN postmeta.meta_key = '_order_total' THEN postmeta.meta_value END) AS 'price'

FROM
wp_posts AS orders
INNER JOIN wp_postmeta AS postmeta
ON orders.ID = postmeta.post_id 

INNER JOIN wp_woocommerce_order_items AS order_name
ON
orders.ID = order_name.order_id 

WHERE orders.post_type = 'shop_order' AND orders.post_status = 'wc-failed'
GROUP BY ID
`

module.exports = {
  emailQuery,
  processingProductQuery,
  assemblyProductQuery,
  readyToDelivery,
  selectCancelledItemsQuery,
  countStatusesQuery,
  readyToDeliveryBy10To12,
  readyToDeliveryBy12To14,
  readyToDeliveryBy14To16,
  readyToDeliveryBy16To18,
  readyToDeliveryBy18To20,
  readyToDeliveryBy20To22,
  failedOrdersQuery
};
