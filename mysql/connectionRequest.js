require("dotenv").config();

module.exports = function () {
  let mysql = require("mysql2");

  let connection = mysql.createConnection({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DB,
    // port: process.env.MYSQL_PORT
  });

  connection.connect((err) => {
    if (err) {
      console.log(`connectionRequest - FAILED: ${err.stack}`);
    } else {
      console.log(`DB connectionRequest - SUCCESSFUL ${connection.threadId}`);
    }
  });

  return connection;
};
