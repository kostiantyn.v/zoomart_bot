const { Scenes } = require("telegraf");
// const pool = require("../mysql").mysql__pool;
const connectionRequest = require("../mysql/connectionRequest");
const { assemblyProductQuery } = require("../queryList");
const mailerInstance = require("../mailer");

const storekeeperScene = new Scenes.BaseScene("storekeeperTypeScene");

storekeeperScene.enter((ctx) => {
  let connection = connectionRequest();

  try {
    connection.query(assemblyProductQuery, function(err, result) { 
      if(err) { 
        console.log(err);
        connection.destroy();
        ctx.scene.leave();
      } 

      if(result.length === 0) { 
        ctx.reply('Заказов нет, попробуйте попозже');
        connection.destroy();
        ctx.scene.leave();
      }

      if(result.length > 0) {
        result.forEach((element) => { 
          ctx.reply(`
Заказ: ${element.ID}
Статус: Сборка
Клиент: ${element.first_name} ${element.last_name}
Телефон: ${element.phone}
Адрес: ${element.address}
Время и дата: ${element.date} ${element.time}
Товар: ${element.order_name}   

Цена: ${element.price} ₸
          `, {
            reply_markup: JSON.stringify({
              inline_keyboard: [
                [{ text: 'Готов к Доставке', callback_data: `${element.ID}` }],
                [{ text: 'Нет в наличии', callback_data: 'not_available'}]
              ]
            })
          });

          storekeeperScene.action(`${element.ID}`, (ctx) => { 
            let poolConnection = connectionRequest();

            try { 
              poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-ready-to-delivery' WHERE ID = ${element.ID}`, function(err, res) {
                if(err) {
                  console.log(err);
                  poolConnection.destroy();
                  ctx.scene.leave();
                } else { 
                  console.log('result: ', res);
                  ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Готов к Доставке

Заказ передан курьеру                  
                  `);
                  poolConnection.destroy();
                  ctx.scene.leave();
                }
              });
            } catch (request__error) {
              console.log(`Something went wrong with request: ${request__error}`)
            }
          });

          storekeeperScene.action("not_available", async (ctx) => {
            try {
              const message = {
                to: `${element.email}`,
                subject: `Отмена заказа - ${element.ID}`,
                text: `Приносим свои извения, но заказ № ${element.ID} в данный момент не может быть обработан`,
              };

              if (message) {
                mailerInstance(message);

                ctx.reply("Сообщение отправлено на почту");

                return ctx.scene.leave();
              }
            } catch (error) {
              console.log(`Something went wrong - ${error}`);
              return ctx.scene.leave();
            }
            console.log(`Блок отмена для - ${element.email}`);
            return await ctx.scene.leave();
          });

          connection.destroy();
        })
      }
    });
  } catch (storekeeper_request_err) {
    console.log(`storekeeper scene request - failed: ${storekeeper_request_err}`);
  }
});

storekeeperScene.leave((ctx) => {
  console.log("Конец сцены - Кладовщик");
});

module.exports = storekeeperScene;
