const { Scenes } = require("telegraf");
const connectionRequest = require("../mysql/connectionRequest");
const { countStatusesQuery } = require("../queryList");

const statusesScene = new Scenes.BaseScene("statusesTypeScene");

statusesScene.enter((ctx) => {
  let connection = connectionRequest();

  try {
    connection.query(countStatusesQuery, function(err, result) {
      if(err) {
        console.log(err);
        connection.destroy();
        ctx.scene.leave();
      }

      if(result.length === 0) { 
        ctx.reply('Статусов нет, попробуйте позже');
        connection.destroy();
        ctx.scene.leave();
      }

      if(result.length > 0) {
        result.forEach((element) => {
          ctx.reply(`
Отмена - ${element.cancelled}
Сборка - ${element.packing}
Доставка - ${element.transporting}
Выполнен - ${element.completed}          
          `);

          connection.destroy();
          ctx.scene.leave();
        });
      }
    });
    
  } catch (error) {
    console.log(`Something went wrong: ${error}`);
  }
});

statusesScene.leave((ctx) => {
  console.log("Конец сцене - Статус");
})

module.exports = statusesScene;