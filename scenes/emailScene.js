const { Markup, Composer, Scenes } = require("telegraf");
const pool = require("../mysql").mysql__pool;
const { emailQuery } = require("../queryList");

const emailScene = new Scenes.BaseScene("emailTypeScene");

const emailCollection = [];

pool.query(emailQuery).then((response) => {
  return response.forEach((element) => {
    return element.forEach((elem) => {
      return emailCollection.push(elem);
    });
  });
});

emailScene.enter((ctx) => ctx.reply(`Здравствуйте, введите свой email:`));
emailScene.on("message", async (ctx, next) => {
  const userText = ctx.update.message.text;
  const userEmail =
  emailCollection.find((email) => email.user_email === userText) || {};
  const value = Object.values(userEmail);

  if (userEmail !== {}) {
    if (value[2].includes("shop_manager") === true) {
     await ctx.sendMessage(
        `Менеджер - ${userEmail.display_name}`,
        Markup.keyboard([["Получить заказы"], ["Статусы"]])
          .oneTime()
          .resize()
      );
    } else {
      return next();
    }
  } else {
    console.log('bad');
  }

  return ctx.scene.leave();
});

emailScene.hears("Получить заказы", (ctx) => {
  console.log('ку-ку 2');
})
module.exports = emailScene;
