const { Scenes } = require("telegraf");
const connectionRequest = require("../mysql/connectionRequest");
const {
  readyToDeliveryBy10To12,
  readyToDeliveryBy12To14,
  readyToDeliveryBy14To16,
  readyToDeliveryBy16To18,
  readyToDeliveryBy18To20,
  readyToDeliveryBy20To22,
} = require("../queryList");

const deliveryScene = new Scenes.BaseScene("deliveryTypeScene");

deliveryScene.enter((ctx) => {
  ctx.reply("Выберете временной интервал", {
    reply_markup: JSON.stringify({
      inline_keyboard: [
        [
          { text: "10:00 - 12:00", callback_data: "1" },
          { text: "12:00 - 14:00", callback_data: "2" },
          { text: "14:00 - 16:00", callback_data: "3" },
        ],
        [
          { text: "16:00 - 18:00", callback_data: "4" },
          { text: "18:00 - 20:00", callback_data: "5" },
          { text: "20:00 - 22:00", callback_data: "6" },
        ],
      ],
    }),
  });

  deliveryScene.action("1", async (ctx) => {
    let connection = connectionRequest();
    try {
      connection.query(readyToDeliveryBy10To12, function (err, result) {
        const orders = result.filter((elem) => elem.time !== null);
        if (err) {
          console.log(`failed: ${err.stack}`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (orders.length === 0) {
          ctx.reply(`Заказов нет, попробуйте позже`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (orders.length > 0) {
          
          orders.forEach((element) => {
            ctx.reply(
              `
Заказ: ${element.ID}
Статус: Готов к Доставке
Клиент: ${element.first_name} ${element.last_name}
Телефон: ${element.phone}
Адрес: ${element.address}
Время и дата: ${element.date} ${element.time}
Товар: ${element.order_name}

Цена: ${element.price} ₸
            `,
              {
                reply_markup: JSON.stringify({
                  inline_keyboard: [
                    [
                      {
                        text: "Транспортировка",
                        callback_data: `${element.ID}`,
                      },
                    ],
                  ],
                }),
              }
            );

            connection.destroy();

            deliveryScene.action(`${element.ID}`, (ctx) => {
              console.log('nhfycgjhnbhjdrf')
              let pool = connectionRequest();
              try {
                console.log('nhfycgjhnbhjdrf_1')
                pool.query(
                  `UPDATE wp_posts SET post_status = 'wc-processing' WHERE ID = ${element.ID}`,
                  function (err, res) {
                    if (err) {
                      console.log("err update: ", err);
                    } else {
                      console.log("result update: ", res);
                      ctx.reply(
                        `
Заказ: ${element.ID}
Статус: Транспортировка

Товар доставляется, после доставки выберете опции
                    `,
                        {
                          reply_markup: JSON.stringify({
                            inline_keyboard: [
                              [
                                {
                                  text: "Выполнен",
                                  callback_data: "completed",
                                },
                              ],
                              [
                                {
                                  text: "Не удался",
                                  callback_data: "cancelled",
                                },
                              ],
                            ],
                          }),
                        }
                      );

                      deliveryScene.action("complete", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-completed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else { 
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Выполнен                               
                              `);
                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (complete_request) {
                          console.log('delivery complete request 1 - failed: ', complete_request);
                        }

                      });

                      deliveryScene.action("cancelled", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-failed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else {
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Не удался

Информация о неудавшемся товаре передана - менеджеру 
                              `);

                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (cancelled_request) {
                          console.log(`delivery cancelled request 1 - failed: ${cancelled_request}`);
                        }
                      });
                    }
                  }
                );

                pool.destroy();
              } catch (update_request_err) {
                console.log(
                  `delivery update action 1 request - failed: ${update_request_err}`
                );
              }
            });
          });
        }
      });
    } catch (request_err) {
      console.log(`delivery action 1 - bad request: ${request_err}`);
    }
  });

  deliveryScene.action("2", async (ctx) => {
    let connection = connectionRequest();
    try {
      connection.query(readyToDeliveryBy12To14, function (err, result) {
        if (err) {
          console.log(`failed: ${err.stack}`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (result.length === 0) {
          ctx.reply(`Заказов нет, попробуйте позже`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (result.length > 0) {
          result.forEach((element) => {
            ctx.reply(
              `
Заказ: ${element.ID}
Статус: Готов к Доставке
Клиент: ${element.first_name} ${element.last_name}
Телефон: ${element.phone}
Адрес: ${element.address}
Время и дата: ${element.date} ${element.time}
Товар: ${element.order_name}

Цена: ${element.price} ₸
            `,
              {
                reply_markup: JSON.stringify({
                  inline_keyboard: [
                    [
                      {
                        text: "Транспортировка",
                        callback_data: `${element.ID}`,
                      },
                    ],
                  ],
                }),
              }
            );

            deliveryScene.action(`${element.ID}`, (ctx) => {
              let pool = connectionRequest();
              try {
                pool.query(
                  `UPDATE wp_posts SET post_status = 'wc-transporting' WHERE ID = ${element.ID}`,
                  function (err, res) {
                    if (err) {
                      console.log("err update: ", err);
                    } else {
                      console.log("result update: ", res);
                      ctx.reply(
                        `
Заказ: ${element.ID}
Статус: Транспортировка

Товар доставляется, после доставки выберете опции
                    `,
                        {
                          reply_markup: JSON.stringify({
                            inline_keyboard: [
                              [
                                {
                                  text: "Выполнен",
                                  callback_data: "completed",
                                },
                              ],
                              [
                                {
                                  text: "Не удался",
                                  callback_data: "cancelled",
                                },
                              ],
                            ],
                          }),
                        }
                      );

                      deliveryScene.action("complete", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-completed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else { 
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Выполнен                               
                              `);
                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (complete_request) {
                          console.log('delivery complete request 2 - failed: ', complete_request);
                        }

                      });

                      deliveryScene.action("cancelled", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-failed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else {
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Не удался

Информация о неудавшемся товаре передана - менеджеру 
                              `);

                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (cancelled_request) {
                          console.log(`delivery cancelled request 2 - failed: ${cancelled_request}`);
                        }
                      });
                    }
                  }
                );

                pool.destroy();
              } catch (update_request_err) {
                console.log(
                  `delivery update action 2 request - failed: ${update_request_err}`
                );
              }
            });
          });

          connection.destroy();
        }
      });
    } catch (request_err) {
      console.log(`delivery action 2 - bad request: ${request_err}`);
    }
  });

  deliveryScene.action("3", async (ctx) => {
    let connection = connectionRequest();
    try {
      connection.query(readyToDeliveryBy14To16, function (err, result) {
        if (err) {
          console.log(`failed: ${err.stack}`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (result.length === 0) {
          ctx.reply(`Заказов нет, попробуйте позже`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (result.length > 0) {
          result.forEach((element) => {
            ctx.reply(
              `
Заказ: ${element.ID}
Статус: Готов к Доставке
Клиент: ${element.first_name} ${element.last_name}
Телефон: ${element.phone}
Адрес: ${element.address}
Время и дата: ${element.date} ${element.time}
Товар: ${element.order_name}

Цена: ${element.price} ₸
            `,
              {
                reply_markup: JSON.stringify({
                  inline_keyboard: [
                    [
                      {
                        text: "Транспортировка",
                        callback_data: `${element.ID}`,
                      },
                    ],
                  ],
                }),
              }
            );

            deliveryScene.action(`${element.ID}`, (ctx) => {
              let pool = connectionRequest();
              try {
                pool.query(
                  `UPDATE wp_posts SET post_status = 'wc-transporting' WHERE ID = ${element.ID}`,
                  function (err, res) {
                    if (err) {
                      console.log("err update: ", err);
                    } else {
                      console.log("result update: ", res);
                      ctx.reply(
                        `
Заказ: ${element.ID}
Статус: Транспортировка

Товар доставляется, после доставки выберете опции
                    `,
                        {
                          reply_markup: JSON.stringify({
                            inline_keyboard: [
                              [
                                {
                                  text: "Выполнен",
                                  callback_data: "completed",
                                },
                              ],
                              [
                                {
                                  text: "Не удался",
                                  callback_data: "cancelled",
                                },
                              ],
                            ],
                          }),
                        }
                      );

                      deliveryScene.action("complete", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-completed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else { 
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Выполнен                               
                              `);
                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (complete_request) {
                          console.log('delivery complete request 3 - failed: ', complete_request);
                        }

                      });

                      deliveryScene.action("cancelled", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-failed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else {
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Не удался

Информация о неудавшемся товаре передана - менеджеру 
                              `);

                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (cancelled_request) {
                          console.log(`delivery cancelled request 3 - failed: ${cancelled_request}`);
                        }
                      });
                    }
                  }
                );

                pool.destroy();
              } catch (update_request_err) {
                console.log(
                  `delivery update action 3 request - failed: ${update_request_err}`
                );
              }
            });
          });

          connection.destroy();
        }
      });
    } catch (request_err) {
      console.log(`delivery action 3 - bad request: ${request_err}`);
    }
  });

  deliveryScene.action("4", async (ctx) => {
    let connection = connectionRequest();
    try {
      connection.query(readyToDeliveryBy16To18, function (err, result) {
        if (err) {
          console.log(`failed: ${err.stack}`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (result.length === 0) {
          ctx.reply(`Заказов нет, попробуйте позже`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (result.length > 0) {
          result.forEach((element) => {
            ctx.reply(
              `
Заказ: ${element.ID}
Статус: Готов к Доставке
Клиент: ${element.first_name} ${element.last_name}
Телефон: ${element.phone}
Адрес: ${element.address}
Время и дата: ${element.date} ${element.time}
Товар: ${element.order_name}

Цена: ${element.price} ₸
            `,
              {
                reply_markup: JSON.stringify({
                  inline_keyboard: [
                    [
                      {
                        text: "Транспортировка",
                        callback_data: `${element.ID}`,
                      },
                    ],
                  ],
                }),
              }
            );

            deliveryScene.action(`${element.ID}`, (ctx) => {
              let pool = connectionRequest();
              try {
                pool.query(
                  `UPDATE wp_posts SET post_status = 'wc-transporting' WHERE ID = ${element.ID}`,
                  function (err, res) {
                    if (err) {
                      console.log("err update: ", err);
                    } else {
                      console.log("result update: ", res);
                      ctx.reply(
                        `
Заказ: ${element.ID}
Статус: Транспортировка

Товар доставляется, после доставки выберете опции
                    `,
                        {
                          reply_markup: JSON.stringify({
                            inline_keyboard: [
                              [
                                {
                                  text: "Выполнен",
                                  callback_data: "completed",
                                },
                              ],
                              [
                                {
                                  text: "Не удался",
                                  callback_data: "cancelled",
                                },
                              ],
                            ],
                          }),
                        }
                      );

                      deliveryScene.action("complete", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-completed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else { 
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Выполнен                               
                              `);
                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (complete_request) {
                          console.log('delivery complete request 4 - failed: ', complete_request);
                        }

                      });

                      deliveryScene.action("cancelled", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-failed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else {
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Не удался

Информация о неудавшемся товаре передана - менеджеру 
                              `);

                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (cancelled_request) {
                          console.log(`delivery cancelled request 4 - failed: ${cancelled_request}`);
                        }
                      });
                    }
                  }
                );

                pool.destroy();
              } catch (update_request_err) {
                console.log(
                  `delivery update action 4 request - failed: ${update_request_err}`
                );
              }
            });
          });

          connection.destroy();
        }
      });
    } catch (request_err) {
      console.log(`delivery action 4 - bad request: ${request_err}`);
    }
  });

  deliveryScene.action("5", async (ctx) => {
    let connection = connectionRequest();
    try {
      connection.query(readyToDeliveryBy18To20, function (err, result) {
        if (err) {
          console.log(`failed: ${err.stack}`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (result.length === 0) {
          ctx.reply(`Заказов нет, попробуйте позже`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (result.length > 0) {
          result.forEach((element) => {
            ctx.reply(
              `
Заказ: ${element.ID}
Статус: Готов к Доставке
Клиент: ${element.first_name} ${element.last_name}
Телефон: ${element.phone}
Адрес: ${element.address}
Время и дата: ${element.date} ${element.time}
Товар: ${element.order_name}

Цена: ${element.price} ₸
            `,
              {
                reply_markup: JSON.stringify({
                  inline_keyboard: [
                    [
                      {
                        text: "Транспортировка",
                        callback_data: `${element.ID}`,
                      },
                    ],
                  ],
                }),
              }
            );

            deliveryScene.action(`${element.ID}`, (ctx) => {
              let pool = connectionRequest();
              try {
                pool.query(
                  `UPDATE wp_posts SET post_status = 'wc-transporting' WHERE ID = ${element.ID}`,
                  function (err, res) {
                    if (err) {
                      console.log("err update: ", err);
                    } else {
                      console.log("result update: ", res);
                      ctx.reply(
                        `
Заказ: ${element.ID}
Статус: Транспортировка

Товар доставляется, после доставки выберете опции
                    `,
                        {
                          reply_markup: JSON.stringify({
                            inline_keyboard: [
                              [
                                {
                                  text: "Выполнен",
                                  callback_data: "completed",
                                },
                              ],
                              [
                                {
                                  text: "Не удался",
                                  callback_data: "cancelled",
                                },
                              ],
                            ],
                          }),
                        }
                      );

                      deliveryScene.action("complete", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-completed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else { 
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Выполнен                               
                              `);
                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (complete_request) {
                          console.log('delivery complete request 5 - failed: ', complete_request);
                        }

                      });

                      deliveryScene.action("cancelled", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-failed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else {
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Не удался

Информация о неудавшемся товаре передана - менеджеру 
                              `);

                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (cancelled_request) {
                          console.log(`delivery cancelled request 5 - failed: ${cancelled_request}`);
                        }
                      });
                    }
                  }
                );

                pool.destroy();
              } catch (update_request_err) {
                console.log(
                  `delivery update action 5 request - failed: ${update_request_err}`
                );
              }
            });
          });

          connection.destroy();
        }
      });
    } catch (request_err) {
      console.log(`delivery action 5 - bad request: ${request_err}`);
    }
  });

  deliveryScene.action("6", async (ctx) => {
    let connection = connectionRequest();
    try {
      connection.query(readyToDeliveryBy20To22, function (err, result) {
        if (err) {
          console.log(`failed: ${err.stack}`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (result.length === 0) {
          ctx.reply(`Заказов нет, попробуйте позже`);
          connection.destroy();
          ctx.scene.leave();
        }

        if (result.length > 0) {
          result.forEach((element) => {
            ctx.reply(
              `
Заказ: ${element.ID}
Статус: Готов к Доставке
Клиент: ${element.first_name} ${element.last_name}
Телефон: ${element.phone}
Адрес: ${element.address}
Время и дата: ${element.date} ${element.time}
Товар: ${element.order_name}

Цена: ${element.price} ₸
            `,
              {
                reply_markup: JSON.stringify({
                  inline_keyboard: [
                    [
                      {
                        text: "Транспортировка",
                        callback_data: `${element.ID}`,
                      },
                    ],
                  ],
                }),
              }
            );

            deliveryScene.action(`${element.ID}`, (ctx) => {
              let pool = connectionRequest();
              try {
                pool.query(
                  `UPDATE wp_posts SET post_status = 'wc-transporting' WHERE ID = ${element.ID}`,
                  function (err, res) {
                    if (err) {
                      console.log("err update: ", err);
                    } else {
                      console.log("result update: ", res);
                      ctx.reply(
                        `
Заказ: ${element.ID}
Статус: Транспортировка

Товар доставляется, после доставки выберете опции
                    `,
                        {
                          reply_markup: JSON.stringify({
                            inline_keyboard: [
                              [
                                {
                                  text: "Выполнен",
                                  callback_data: "completed",
                                },
                              ],
                              [
                                {
                                  text: "Не удался",
                                  callback_data: "cancelled",
                                },
                              ],
                            ],
                          }),
                        }
                      );

                      deliveryScene.action("complete", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-completed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else { 
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Выполнен                               
                              `);
                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (complete_request) {
                          console.log('delivery complete request 6 - failed: ', complete_request);
                        }

                      });

                      deliveryScene.action("cancelled", (ctx) => {
                        let poolConnection = connectionRequest();

                        try {
                          poolConnection.query(`UPDATE wp_posts SET post_status = 'wc-failed' WHERE ID = ${element.ID}`, function(err, res) {
                            if(err) {
                              console.log(err);
                              poolConnection.destroy();
                            } else {
                              ctx.reply(`
Заказ: ${element.ID}
Статус изменён на: Не удался

Информация о неудавшемся товаре передана - менеджеру 
                              `);

                              poolConnection.destroy();
                              ctx.scene.leave();
                            }
                          });
                        } catch (cancelled_request) {
                          console.log(`delivery cancelled request 6 - failed: ${cancelled_request}`);
                        }
                      });
                    }
                  }
                );

                pool.destroy();
              } catch (update_request_err) {
                console.log(
                  `delivery update action 6 request - failed: ${update_request_err}`
                );
              }
            });
          });

          connection.destroy();
        }
      });
    } catch (request_err) {
      console.log(`delivery action 6 - bad request: ${request_err}`);
    }
  });

});

deliveryScene.leave((ctx) => {
  console.log("Конец сцены - Доставщик");
});

module.exports = deliveryScene;
