const { Scenes } = require("telegraf");
// const pool = require("../mysql").mysql__pool;
const connectionRequest = require("../mysql/connectionRequest");
const { processingProductQuery } = require("../queryList");
const mailerInstance = require("../mailer");
const managerScene = new Scenes.BaseScene("managerTypeScene");

managerScene.enter((ctx) => {
  let connection = connectionRequest();
  try {
    connection.query(processingProductQuery, function (err, result) {
      if (err) {
        console.log(err);
        connection.destroy();
      }

      if (result.length === 0) {
        ctx.reply("Заказов нет, попробуйте позже");
        connection.destroy();
        ctx.scene.leave();
      }

      if (result.length > 0) {
        result.forEach((element) => {
          ctx.reply(
            `
Заказ: ${element.ID}
Статус: Обработка
Клиент: ${element.first_name} ${element.last_name}
Телефон: ${element.phone}
Адрес: ${element.address}
Время и дата: ${element.data} ${element.time}
Товар: ${element.order_name}

Цена: ${element.price} ₸
          `,
            {
              reply_markup: JSON.stringify({
                inline_keyboard: [
                  [
                    { text: "Сборка", callback_data: `${element.ID}` },
                    { text: "Отмена", callback_data: "cancel" },
                  ],
                ],
              }),
            }
          );

          managerScene.action(`${element.ID}`, async (ctx) => {
            console.log("Сборка");
            try {
              connection.query(
                `UPDATE wp_posts SET post_status = 'wc-packing' WHERE ID = ${element.ID}`,
                function (err, res) {
                  if (err) {
                    console.log("err: ", err);
                    connection.destroy();
                  } else {
                    console.log("update result: ", res);
                    connection.destroy();
                    ctx.reply(`
Заказ: ${element.ID}                  
Статус изменён на: Сборка

Заказ передан кладовщику
                  `);
                    ctx.scene.leave();
                  }
                }
              );
            } catch (request_err) {
              console.log(`manager action request bad: ${request_err}`);
            }
          });

          managerScene.action("cancel", async (ctx) => {
            try {
              const message = {
                to: `${element.email}`,
                subject: `Отмена заказа - ${element.ID}`,
                text: `Приносим свои извения, но заказ № ${element.ID} в данный момент не может быть обработан`,
              };

              if (message) {
                mailerInstance(message);

                ctx.reply("Сообщение отправлено на почту");

                return ctx.scene.leave();
              }
            } catch (error) {
              console.log(`Something went wrong - ${error}`);
              return ctx.scene.leave();
            }
            console.log(`Блок отмена для - ${element.email}`);
            return await ctx.scene.leave();
          });
        });
      }
    });
  } catch (err) {
    console.log(`managerScene catch err: ${err}`);
  }
});

managerScene.leave((ctx) => {
  console.log("Конец");
});

module.exports = managerScene;
