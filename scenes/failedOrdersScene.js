const { Scenes } = require("telegraf");
// const pool = require("../mysql").mysql__pool;
const connectionRequest = require("../mysql/connectionRequest");
const { failedOrdersQuery } = require("../queryList");
const failedScene = new Scenes.BaseScene("failedTypeScene");

failedScene.enter((ctx) => {
  let connection = connectionRequest();
  try {
    connection.query(failedOrdersQuery, function (err, result) {
      if (err) {
        console.log("err: ", err);
        connection.destroy();
        ctx.scene.leave();
      }

      if (result.length === 0) {
        ctx.reply("Товаров нет, попробуйте позже");
        connection.destroy();
        console.log("Конец сцены");
        ctx.scene.leave();
      }

      if (result.length > 0) {
        result.forEach((element) => {
          ctx.reply(
            `
Заказ: ${element.ID}
Статус: Не удался
Клиент: ${element.first_name} ${element.last_name}
Телефон: ${element.phone}
Адрес: ${element.address}
Время и дата: ${element.date} ${element.time}
Товар: ${element.order_name}    

Цена: ${element.price} ₸   
          `,
            {
              reply_markup: JSON.stringify({
                inline_keyboard: [
                  [{ text: "Изменить", callback_data: `${element.ID}` }],
                ],
              }),
            }
          );

          failedScene.action(`${element.ID}`, (ctx) => {
            let poolConnection = connectionRequest();
            ctx.reply(
              "Введите интервал времени доставки в формате - (xx:xx - xx:xx)"
            );
            failedScene.on("text", (ctx) => {
              try {
                poolConnection.query(
                  `
                UPDATE wp_postmeta 
                INNER JOIN wp_posts AS posts
                ON posts.ID = wp_postmeta.post_id
                
                SET wp_postmeta.meta_value = '${ctx.update.message.text}', posts.post_status = 'wc-ready-to-delivery' WHERE wp_postmeta.meta_key = 'delivery_time' AND posts.ID = ${element.ID}                
                `,
                  function (err, res) {
                    if (err) {
                      console.log(err);
                      poolConnection.destroy();
                      ctx.scene.leave();
                    } else {
                      console.log("res: ", res);
                      ctx.reply(`
Товар: ${element.ID}
Статус заказа: Готов к Доставке

Заказ успешно изменён
                    `);
                      poolConnection.destroy();
                      ctx.scene.leave();
                    }
                  }
                );
              } catch (err) {
                console.log(`Error: ${err}`);
              }
            });
          });
          connection.destroy();
        });
      }
    });
  } catch (error) {
    console.log(`Something went wrong: ${error}`);
  }
});

module.exports = failedScene;

// failedScene.action(`${element.ID}`, (ctx) => {
//   try {
//     pool.query(
//       `UPDATE wp_posts SET post_status = 'wc-ready-to-delivery' WHERE ID = ${element.ID}`
//     )
//     .then((response) => {
//       console.log("update response: ", response);
//       ctx.reply(`
// Заказ: ${element.ID}
// Статус изменён на: Готов к Доставке

// Заказ передан курьеру
//       `)

//       return ctx.scene.leave();
//     })
//   } catch (error) {
//     console.log(`Something went wrong: ${error}`);
//   }
//   console.log("test button - work");
// })
