const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport(
  {
    host: "smtp.yandex.ru",
    port: 465,
    secure: true,
    auth: {
      user: "info@zoomart.kz",
      pass: "zoo03mart09kz12",
    },
  },
  {
    from: "zooMart <info@zoomart.kz>",
  }
);

const mailerInstance = message => {
  transporter.sendMail(message, (error, info) => {
    if(error) return console.log('mail error: ', error);

    console.log('Email sent: ', info);
  });
}

module.exports = mailerInstance;
