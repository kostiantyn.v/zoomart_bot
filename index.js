const { Telegraf, session, Scenes, Markup } = require("telegraf");
// const pool = require("./mysql").mysql__pool;
const connectionRequest = require("./mysql/connectionRequest");
const managerScene = require("./scenes/managerScene");
const statusesScene = require("./scenes/statusesTypeScene");
const storekeeperScene = require("./scenes/storekeeperScene");
const deliveryScene = require("./scenes/deliveryScene");
const failedScene = require("./scenes/failedOrdersScene");
const { emailQuery } = require("./queryList");
require("dotenv").config();

const bot = new Telegraf(process.env.TELEGRAM_API);
const emailScene = new Scenes.BaseScene("email");
const stage = new Scenes.Stage([
  emailScene,
  managerScene,
  statusesScene,
  storekeeperScene,
  deliveryScene,
  failedScene,
]);
bot.use(session());
bot.use(stage.middleware());

const userRoles = (chat_id, user) => {
  const value = Object.values(user);
  console.log("value: ", value[2]);

  if (value[2].includes("shop_manager") === true) {
    bot.telegram.sendMessage(
      chat_id,
      `Менеджер - ${user.display_name}`,
      Markup.keyboard([
        ["Получить заказы"],
        ["Статусы"],
        ["Неудавшиеся заказы"],
      ])
        .oneTime()
        .resize()
    );

    bot.hears("Получить заказы", (ctx) => {
      ctx.scene.enter("managerTypeScene");
      console.log("Получить заказы блок");
    });

    bot.hears("Статусы", async (ctx) => {
      await ctx.scene.enter("statusesTypeScene");
      console.log("Получить cтатусы блок");
    });

    bot.hears("Неудавшиеся заказы", async (ctx) => {
      ctx.scene.enter("failedTypeScene");
      console.log("Неудавшиеся заказы - старт сцены");
    });
  }

  if (value[2].includes("shop_storekeeper") === true) {
    bot.telegram.sendMessage(
      chat_id,
      `Кладовщик - ${user.display_name}`,
      Markup.keyboard([["Заказы"]])
        .oneTime()
        .resize()
    );

    bot.hears("Заказы", async (ctx) => {
      await ctx.scene.enter("storekeeperTypeScene");
      console.log("Кладовщик");
    });
  }

  if (value[2].includes("shop_deliver") === true) {
    bot.telegram.sendMessage(
      chat_id,
      `Курьер - ${user.display_name}`,
      Markup.keyboard([["Получить товары"]])
        .oneTime()
        .resize()
    );

    bot.hears("Получить товары", async (ctx) => {
      await ctx.scene.enter("deliveryTypeScene");
      console.log("Доставщик");
    });
  }
};

bot.command("start", (ctx) => ctx.scene.enter("email"));
emailScene.enter((ctx) => ctx.reply(`Здравствуйте, введите свой email`));
const emailCollection = [];

let connection = connectionRequest();

connection.query(emailQuery, function (err, res) {
  if (err) {
    console.log(err);
    connection.destroy();
  } else {
    console.log(res);
    res.forEach((elem) => {
      emailCollection.push(elem);
      return connection.destroy();
    });
  }
});

emailScene.on("message", (ctx) => {
  const userEmail =
    emailCollection.find(
      (elem) => elem.user_email === ctx.update.message.text
    ) || {};

  if (userEmail !== {}) {
    console.log("user: ", userEmail);
    userRoles(ctx.chat.id, userEmail);
  }
  ctx.scene.leave();
});

bot.launch();
